interface Rectangle {
    height: number,
    width: number
}

interface ColorRectangle extends Rectangle{
    color : string
}

const rectangle: Rectangle = {
   height: 20,
   width: 10
};

const ColorRedRectangle: ColorRectangle = {
    height: 20,
    width: 10,
    color: "red"
 };

 console.log(ColorRedRectangle);

console.log(rectangle);